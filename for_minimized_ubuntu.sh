#!/bin/bash

# Устанавливаем переменную для режима non-interactive
export DEBIAN_FRONTEND=noninteractive

# Определяем версию Ubuntu
UBUNTU_VERSION=$(lsb_release -rs)

# Обновляем систему и устанавливаем базовые пакеты
apt update && apt upgrade -y
apt install iputils-ping iputils-tracepath unzip zip mc nano tmux cron bash-completion less ncdu fzf bmon tldr -y

# Проверяем версию и устанавливаем eza или exa
if dpkg --compare-versions "$UBUNTU_VERSION" ge "24.04"; then
    apt install eza -y
    # Обновляем алиасы для eza
    sed -i 's/ls --color=auto/eza/' .bashrc
    sed -i 's/ls -alF/eza -alF/' .bashrc
else
    apt install exa -y
    # Обновляем алиасы для exa
    sed -i 's/ls --color=auto/exa/' .bashrc
    sed -i 's/ls -alF/exa -alF/' .bashrc
fi

# Исправляем другие алиасы для ls
sed -i 's/ls -A/\/bin\/ls -A --color=auto/' .bashrc
sed -i 's/ls -CF/\/bin\/ls --color=auto/' .bashrc

# Настраиваем tmux
cat << EOF > .tmux.conf
set -g default-terminal "screen-256color"
set -g mouse on
EOF
sudo chown $SUDO_USER:$SUDO_USER .tmux.conf
sudo chmod 660 .tmux.conf

# Загружаем автодополнение для tmux
curl https://raw.githubusercontent.com/imomaliev/tmux-bash-completion/master/completions/tmux > /usr/share/bash-completion/completions/tmux

# Обновляем bash-completion в .bashrc
if ! grep -q "source /etc/profile.d/bash_completion.sh" .bashrc; then
    echo "source /etc/profile.d/bash_completion.sh" >> .bashrc
fi

# Создаем и настраиваем директории для Midnight Commander
mkdir -p .config/mc
curl -o .config/mc/ini https://gitlab.com/cyber_watcher/usefulbashscripts/-/raw/main/mc/ini
sudo chown -R $SUDO_USER:$SUDO_USER .config

# Завершаем
echo "Установка завершена. Перезапустите терминал для применения изменений."
